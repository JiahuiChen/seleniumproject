﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using Selenium;


namespace seleniumProject
{
    [TestFixture]
    public class logintest
    {
        private ISelenium selenium;
        private StringBuilder verificationErrors;

        [SetUp]
        public void SetupTest()
        {
            selenium = new DefaultSelenium("localhost", 4444, "*chrome", "https://mash.virginhealthmiles.com/");
            selenium.Start();
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                selenium.Stop();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void TheLogintestTest()
        {
            selenium.Open("/Login.aspx");
            selenium.Type("id=oUserID", "testsm0001");
            selenium.Type("id=txtPlainPassword", "Password");
            selenium.Type("id=oPwdID", "a1111111");
            selenium.Click("id=oLogon");
            selenium.WaitForPageToLoad("30000");
            selenium.Click("link=Activity Journal");
            selenium.WaitForPageToLoad("30000");
            selenium.Click("id=_ctl0_uxHeader_uxMainMenu__ctl0_uxSubMenu__ctl1_lnkNavItem");
            selenium.WaitForPageToLoad("30000");
            selenium.Click("id=_ctl0_uxHeader_uxMainMenu__ctl0_uxSubMenu__ctl0_lnkNavItem");
            selenium.WaitForPageToLoad("30000");
            selenium.Click("id=_ctl0_uxHeader_uxMainMenu__ctl2_uxSubMenu__ctl0_lnkNavItem");
            selenium.WaitForPageToLoad("30000");
            selenium.Click("id=_ctl0_uxHeader_uxMainMenu__ctl4_uxSubMenu__ctl1_lnkNavItem");
            selenium.WaitForPageToLoad("30000");
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");
            selenium.Click("link=Log Out");
            selenium.WaitForPageToLoad("30000");
        }
    }
}
