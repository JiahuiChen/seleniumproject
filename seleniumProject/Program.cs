﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;
using System.Threading;
namespace seleniumProject
{
    public class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("https://mash.virginhealthmiles.com/login.aspx");
            WebDriverWait loginwait = new WebDriverWait(driver, TimeSpan.FromSeconds(3));
            //loginwait.Until(d => d.FindElement(By.Id("txtPlainPassword")));
            IWebElement login = driver.FindElement(By.Id("oUserID"));
            login.Clear();
            login.SendKeys("testsm0001");
            driver.FindElement(By.Id("txtPlainPassword")).SendKeys("a1111111");
            IWebElement psd = driver.FindElement(By.Id("txtPlainPassword"));
            psd.Clear();
            psd.SendKeys("a1111111");
            //IWebElement pswSection = driver.FindElement(By.Id("loginSection")).FindElement(By.XPath("//table/tbody/tr[3]/td"));
            //pswSection.Click();
            //IWebElement selectElement = loginwait.Until(ExpectedConditions.ElementIsVisible(By.Id("txtPlainPassword")));
            ////psw.Clear();
            //IWebElement psw = pswSection.FindElement(By.Id("txtPlainPassword"));
            //psw.Clear();
            //psw.Click();
            //psw.SendKeys("a1111111");
            //psw = driver.FindElement(By.Id("txtPlainPassword"));
            //IJavaScriptExecutor js;
            //js.ExecuteScript("document.getElementById('x').focus()");
            driver.FindElement(By.Id("oLogon")).Click();
            
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            wait.Until(d => d.FindElement(By.Id("measurementSideNav")));

            //getShopForGoodPage(driver);
            //getMonthStatementPage(driver);
            //enterActivityJourney(driver);
            Random rn = new Random();
            int rweight = rn.Next(120, 300);
            int rbfat = rn.Next(7, 30);
            string date = System.DateTime.Now.ToString();
            enterPersonalChallenge(driver, date);
            //enterBioManualEntry(driver, rweight.ToString(), rbfat.ToString());
            //getBioMeasurement(driver);
            //personalChallengeSteps1(driver);
            ////Name and Details
            //personalChallengeSteps2(driver, date);
            //////Who would you like to invite?
            //personalChallengeSteps3(driver);
            //////Ready to send?
            //personalChallengeSteps4(driver);
            //personalChallengeSteps5(driver);
            //getNewsFeed(driver);
            //string enternewsfeed = enterNewsFeed(driver, date);

            //enterPersonalChallenge(driver, date);

            //getSupport(driver);
            //if(driver.Title.Equals("Virgin Pulse - Shop For Products"))
               // error
            /*
            foreach(IWebElement option in headermenu)
            {
                option.Click();
                string tagName = option.FindElement(By.TagName("span")).Text;
                switch (tagName)
                {
                    case "Activity":
                        //option.FindElement(By.Id("_ctl0_uxHeader_uxMainMenu__ctl1_uxSubMenu__ctl0_lnkNavItem")).Click();
                        IList<IWebElement> tabs = option.FindElement(By.ClassName("dropdown-menu")).FindElements(By.TagName("li"));
                        foreach (IWebElement sub in tabs)
                        {
                            try
                            {
                                if (sub.FindElement(By.TagName("a")).Text.Equals("Activity Journal"))
                                {
                                    sub.Click();
                                    WebDriverWait activitywait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
                                    activitywait.Until(d => d.FindElement(By.Id("tabs")));

                                    Random rn = new Random();
                                    //create activity entry
                                    int ractivity = rn.Next(2,44);
                                    SelectElement acselect = new SelectElement(driver.FindElement(By.Id("AL-ActivityList")));
                                    acselect.SelectByIndex(ractivity);
                                    int rhour = rn.Next(0, 3);
                                    driver.FindElement(By.Id("txtALActivityHours")).SendKeys(rhour.ToString());
                                    int rmin = rn.Next(0, 60);
                                    driver.FindElement(By.Id("txtALActivityMinutes")).SendKeys(rmin.ToString());
                                    int rintensity = rn.Next(0, 3);
                                    SelectElement intensity = new SelectElement(driver.FindElement(By.Id("ddAlIntensity")));
                                    intensity.SelectByIndex(rintensity);
                                    driver.FindElement(By.Id("AL-btnSave")).Click();

                                    //update activity entry
                                    IWebElement te = driver.FindElement(By.Id("AL-Wrapper")).FindElement(By.XPath("//table/tbody/tr[1]/td[4]"));
                                    IList<IWebElement> buttons = te.FindElements(By.TagName("button"));
                                    buttons[0].Click();
                                    //IWebElement showtable = driver.FindElement(By.Id("AL-Wrapper")).FindElement(By.TagName("table"));
                                    //showtable.FindElement(By.XPath("tbody/tr[1]/td[4]")).FindElement(By.ClassName("edit-record button-nostyle")).Click();
                                    ractivity = rn.Next(2, 44);
                                    SelectElement upacselect = new SelectElement(driver.FindElement(By.Id("AL-UpdateActivityList")));
                                    upacselect.SelectByIndex(ractivity);
                                    rhour = rn.Next(0, 3);
                                    driver.FindElement(By.Id("input-durationHours")).SendKeys(rhour.ToString());
                                    rmin = rn.Next(0, 60);
                                    driver.FindElement(By.Id("input-durationMinutes")).SendKeys(rmin.ToString());
                                    rintensity = rn.Next(0, 3);
                                    SelectElement upintensity = new SelectElement(driver.FindElement(By.Id("ddALIntensityEditList")));
                                    upintensity.SelectByIndex(rintensity);
                                    string upCal = driver.FindElement(By.Id("input-calories")).Text;
                                    driver.FindElement(By.Id("AL-Save-Edit")).Click();

                                    //compare the value
                                    IList<IWebElement> tds = driver.FindElement(By.Id("view-row_0")).FindElements(By.TagName("td"));
                                    string upCalAfter = tds[2].Text;
                                    if (upCal.Equals(upCalAfter))
                                    {
                                        te.FindElement(By.ClassName("delete-record")).Click();
                                        driver.SwitchTo().Alert().Accept();
                                    }
                                }
                            }
                            catch
                            {
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            //driver.FindElement(By.Id("oLogon")).Click();
            */

            // Should see: "Cheese - Google Search"
            System.Console.WriteLine("Page title is: " + driver.Title);
            //System.Console.ReadKey();
            //Close the browser
            driver.Quit();
        }

        public static string getShopForGoodPage(IWebDriver driver)
        {
            IWebDriver dr = driver;
            IJavaScriptExecutor js = dr as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollTo(0, 0);"); 
            IWebElement header = driver.FindElement(By.ClassName("VHMheadermenuc"));
            //header.FindElement(By.XPath("//div[1]/ul/li[1]/ul/li[0]")).Click();
            IList<IWebElement> headermenu = header.FindElements(By.ClassName("menu-item-li"));
            headermenu[0].Click();
            IList<IWebElement> tabs = headermenu[0].FindElement(By.ClassName("dropdown-menu")).FindElements(By.TagName("li"));
            tabs[0].Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            wait.Until(d => d.FindElement(By.Id("wrapper")));
            Thread.Sleep(3000);
            return driver.Title;
        }

        public static string personalChallengeSteps1(IWebDriver driver)
        {
            IWebDriver dr = driver;
            IJavaScriptExecutor js = dr as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollTo(0, 0);");
            IWebElement header = driver.FindElement(By.ClassName("VHMheadermenuc"));
            IList<IWebElement> headermenu = header.FindElements(By.ClassName("menu-item-li"));
            headermenu[3].Click();
            IList<IWebElement> tabs = headermenu[3].FindElement(By.ClassName("dropdown-menu")).FindElements(By.TagName("li"));
            tabs[1].Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            wait.Until(d => d.FindElement(By.Id("challengeBody")));
            Thread.Sleep(4000);
            js.ExecuteScript("location.reload();");
            Thread.Sleep(4000);
            IList<IWebElement> cates = driver.FindElement(By.Id("challengeBody")).FindElement(By.ClassName("row")).FindElement(By.ClassName("well")).FindElements(By.TagName("div"));
            IList<IWebElement> activities = cates[0].FindElement(By.TagName("ul")).FindElements(By.TagName("li"));
            activities[0].Click();
            Thread.Sleep(4000);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            wait.Until(d => d.FindElement(By.Id("divIntro")));
            dr = driver;
            js = dr as IJavaScriptExecutor;
            js.ExecuteScript("__doPostBack('_ctl0$MainContentPlaceHolder$btnIndiv','')");

            Thread.Sleep(3000);
            IList<IWebElement> chas = driver.FindElement(By.Id("divStepOne")).FindElement(By.Id("divStepOneSettings")).FindElements(By.TagName("div"));
            string outway = chas[2].FindElement(By.Id("btnStepsFirst")).GetAttribute("value");
            //Be the first to reach 100,000 steps
            return outway;
        }

        public static string personalChallengeSteps2(IWebDriver driver, string date)
        {
            Thread.Sleep(3000);
            IList<IWebElement> chas = driver.FindElement(By.Id("divStepOne")).FindElement(By.Id("divStepOneSettings")).FindElements(By.TagName("div"));
            chas[2].FindElement(By.Id("btnStepsFirst")).Click();
            Thread.Sleep(1000);
            //IList<IWebElement> tds = driver.FindElement(By.Id("divSave")).FindElement(By.TagName("table")).FindElement(By.TagName("tbody")).FindElement(By.TagName("tr")).FindElements(By.TagName("td"));
            //tds[1].FindElement(By.Id("divSave1")).FindElement(By.Id("btnNext")).Click();
            IWebDriver dr = driver;
            IJavaScriptExecutor js = dr as IJavaScriptExecutor; 
            js.ExecuteScript("document.getElementById('btnNext').click();");
            Thread.Sleep(5000);

            //Thread.Sleep(4000);
            driver.FindElement(By.Id("txtName")).Clear();
            Thread.Sleep(1000);
            driver.FindElement(By.Id("txtName")).SendKeys(date + " test");
            Thread.Sleep(1000);
            driver.FindElement(By.Id("txtDetails")).Clear();
            Thread.Sleep(1000);
            driver.FindElement(By.Id("txtDetails")).SendKeys("blar");
            Thread.Sleep(1000);
            IList<IWebElement> divs = driver.FindElement(By.Id("divStepTwo")).FindElements(By.TagName("div"));
            string outway = divs[0].FindElement(By.TagName("h3")).Text;
            return outway;
        }

        public static string personalChallengeSteps3(IWebDriver driver)
        {
            //IList<IWebElement> tds = driver.FindElement(By.Id("divSave")).FindElement(By.TagName("table")).FindElement(By.TagName("tbody")).FindElement(By.TagName("tr")).FindElements(By.TagName("td"));
            //tds[1].FindElement(By.Id("divSave1")).FindElement(By.Id("btnNext")).Click();
            IWebDriver dr = driver;
            IJavaScriptExecutor js = dr as IJavaScriptExecutor;
            js.ExecuteScript("document.getElementById('btnNext').click();");
            Thread.Sleep(5000);            
            IList<IWebElement> divs = driver.FindElement(By.Id("divStepThree")).FindElements(By.TagName("div"));
            string outway = divs[0].FindElement(By.TagName("h3")).Text;
            return outway;
        }
        public static string personalChallengeSteps5(IWebDriver driver)
        {
            //IList<IWebElement> tds = driver.FindElement(By.Id("divSave")).FindElement(By.TagName("table")).FindElement(By.TagName("tbody")).FindElement(By.TagName("tr")).FindElements(By.TagName("td"));
            //tds[1].FindElement(By.Id("divSave1")).FindElement(By.Id("btnNext")).Click();
            Thread.Sleep(10000);
            //string header = driver.FindElement(By.Id("extendableHeader")).FindElement(By.Id("extendableHeader_sub")).FindElement(By.ClassName("middle")).FindElement(By.Id("contentHeader")).Text;
            //if (!header.StartsWith("Manage"))
            //{
                IWebDriver dr = driver;
                IJavaScriptExecutor js = dr as IJavaScriptExecutor;
                js.ExecuteScript("document.getElementById('btnNext').click();");
                Thread.Sleep(10000);
            //}
            //return driver.FindElement(By.Id("extendableHeader")).FindElement(By.Id("extendableHeader_sub")).FindElement(By.ClassName("middle")).FindElement(By.Id("contentHeader")).Text;
            //return driver.PageSource;
            return driver.Title;
        }
        public static string personalChallengeSteps4(IWebDriver driver)
        {
            //IList<IWebElement> tds = driver.FindElement(By.Id("divSave")).FindElement(By.TagName("table")).FindElement(By.TagName("tbody")).FindElement(By.TagName("tr")).FindElements(By.TagName("td"));
            //tds[1].FindElement(By.Id("divSave1")).FindElement(By.Id("btnNext")).Click();
            //Thread.Sleep(8000);
            IWebDriver dr = driver;
            IJavaScriptExecutor js = dr as IJavaScriptExecutor;
            js.ExecuteScript("document.getElementById('btnNext').click();");
            Thread.Sleep(5000);
            IList<IWebElement> divs = driver.FindElement(By.Id("divReview")).FindElements(By.TagName("div"));
            string outway = divs[0].FindElement(By.TagName("h3")).Text;
            return outway;
        }

        public static string enterPersonalChallenge(IWebDriver driver, string date)
        {
            IWebDriver dr = driver;
            IJavaScriptExecutor js = dr as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollTo(0, 0);"); 
            IWebElement header = driver.FindElement(By.ClassName("VHMheadermenuc"));
            IList<IWebElement> headermenu = header.FindElements(By.ClassName("menu-item-li"));
            headermenu[3].Click();
            IList<IWebElement> tabs = headermenu[3].FindElement(By.ClassName("dropdown-menu")).FindElements(By.TagName("li"));
            tabs[1].Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            wait.Until(d => d.FindElement(By.Id("challengeBody")));
            Thread.Sleep(4000);
            js.ExecuteScript("location.reload();");
            Thread.Sleep(4000);
            IList<IWebElement> cates = driver.FindElement(By.Id("challengeBody")).FindElement(By.ClassName("row")).FindElement(By.ClassName("well")).FindElements(By.TagName("div"));
            IList<IWebElement> activities = cates[0].FindElement(By.TagName("ul")).FindElements(By.TagName("li"));
            activities[0].Click();
            Thread.Sleep(4000);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            wait.Until(d => d.FindElement(By.Id("divIntro")));
            dr = driver;
            js = dr as IJavaScriptExecutor;
            js.ExecuteScript("__doPostBack('_ctl0$MainContentPlaceHolder$btnIndiv','')");

            Thread.Sleep(3000);
            IList<IWebElement> chas = driver.FindElement(By.Id("divStepOne")).FindElement(By.Id("divStepOneSettings")).FindElements(By.TagName("div"));
            chas[2].FindElement(By.Id("btnStepsFirst")).Click();
            Thread.Sleep(1000);
            //IList<IWebElement> tds = driver.FindElement(By.Id("divSave")).FindElement(By.TagName("table")).FindElement(By.TagName("tbody")).FindElement(By.TagName("tr")).FindElements(By.TagName("td"));
            //tds[1].FindElement(By.Id("divSave1")).FindElement(By.Id("btnNext")).Click();
            //Thread.Sleep(4000);
            //driver.FindElement(By.Id("divStepOne")).
            //IList<IWebElement> divs = driver.FindElement(By.Id("divStepTwo")).FindElements(By.TagName("div"));
            //if (divs[0].FindElement(By.TagName("h3")).Text != "Name and Details")
            //{
            js.ExecuteScript("document.getElementById('btnNext').click();");
            Thread.Sleep(5000);
            //}
            
            
            driver.FindElement(By.Id("txtName")).Clear();
            Thread.Sleep(1000);
            driver.FindElement(By.Id("txtName")).SendKeys(date + " test");
            Thread.Sleep(1000);
            driver.FindElement(By.Id("txtDetails")).Clear();
            Thread.Sleep(1000);
            driver.FindElement(By.Id("txtDetails")).SendKeys("blar");
            Thread.Sleep(1000);
            //tds = driver.FindElement(By.Id("divSave")).FindElement(By.TagName("table")).FindElement(By.TagName("tbody")).FindElement(By.TagName("tr")).FindElements(By.TagName("td"));
            //tds[1].FindElement(By.Id("divSave1")).FindElement(By.Id("btnNext")).Click();
            //Thread.Sleep(4000);
            //divs = driver.FindElement(By.Id("divStepThree")).FindElements(By.TagName("div"));
            //if (divs[0].FindElement(By.TagName("h3")).Text != "Who would you like to invite?")
            //{
                js.ExecuteScript("document.getElementById('btnNext').click();");
                Thread.Sleep(5000);
            //}

            //tds = driver.FindElement(By.Id("divSave")).FindElement(By.TagName("table")).FindElement(By.TagName("tbody")).FindElement(By.TagName("tr")).FindElements(By.TagName("td"));
            //tds[1].FindElement(By.Id("divSave1")).FindElement(By.Id("btnNext")).Click();
            //Thread.Sleep(4000);

            //divs = driver.FindElement(By.Id("divReview")).FindElements(By.TagName("div"));
            //if (divs[0].FindElement(By.TagName("h3")).Text != "Ready to send?")
            //{
                js.ExecuteScript("document.getElementById('btnNext').click();");
                Thread.Sleep(5000);
            //}
            //tds = driver.FindElement(By.Id("divSave")).FindElement(By.TagName("table")).FindElement(By.TagName("tbody")).FindElement(By.TagName("tr")).FindElements(By.TagName("td"));
            //tds[1].FindElement(By.Id("divSave1")).FindElement(By.Id("btnNext")).Click();
            //Thread.Sleep(5000);
            //string headerContent = driver.FindElement(By.Id("extendableHeader")).FindElement(By.Id("extendableHeader_sub")).FindElement(By.ClassName("middle")).FindElement(By.Id("contentHeader")).Text;
            //if (!headerContent.StartsWith("Manage"))
            //{
                js.ExecuteScript("document.getElementById('btnNext').click();");
                Thread.Sleep(5000);
            //}
            //return driver.FindElement(By.Id("extendableHeader")).FindElement(By.Id("extendableHeader_sub")).FindElement(By.ClassName("middle")).FindElement(By.Id("contentHeader")).Text;
            return driver.Title;
        }

        public static string getMonthStatementPage(IWebDriver driver)
        {
            IWebDriver dr = driver;
            IJavaScriptExecutor js = dr as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollTo(0, 0);"); 
            IWebElement header = driver.FindElement(By.ClassName("VHMheadermenuc"));
            //header.FindElement(By.XPath("//div[1]/ul/li[1]/ul/li[0]")).Click();
            IList<IWebElement> headermenu = header.FindElements(By.ClassName("menu-item-li"));
            headermenu[0].Click();
            IList<IWebElement> tabs = headermenu[0].FindElement(By.ClassName("dropdown-menu")).FindElements(By.TagName("li"));
            tabs[1].Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            wait.Until(d => d.FindElement(By.Id("vhm_loadca")));
            Thread.Sleep(3000);
            return driver.Title;
        }

        public static void getBioMeasurement(IWebDriver driver)
        {
            IWebElement header = driver.FindElement(By.ClassName("VHMheadermenuc"));
            //header.FindElement(By.XPath("//div[1]/ul/li[1]/ul/li[0]")).Click();
            IList<IWebElement> headermenu = header.FindElements(By.ClassName("menu-item-li"));
            headermenu[2].Click();
            IList<IWebElement> tabs = headermenu[2].FindElement(By.ClassName("dropdown-menu")).FindElements(By.TagName("li"));
            tabs[0].Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            wait.Until(d => d.FindElement(By.Id("btnManualEntry")));
            Thread.Sleep(1000);
        }

        public static string enterBioManualEntry(IWebDriver driver, string weight, string bfat)
        {
            IWebDriver dr = driver;
            IJavaScriptExecutor js = dr as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollTo(0, 0);");
            IWebElement header = driver.FindElement(By.ClassName("VHMheadermenuc"));
            //header.FindElement(By.XPath("//div[1]/ul/li[1]/ul/li[0]")).Click();
            IList<IWebElement> headermenu = header.FindElements(By.ClassName("menu-item-li"));
            headermenu[2].Click();
            IList<IWebElement> tabs = headermenu[2].FindElement(By.ClassName("dropdown-menu")).FindElements(By.TagName("li"));
            tabs[0].Click();
            Thread.Sleep(3000);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            wait.Until(d => d.FindElement(By.Id("btnManualEntry")));
            //driver.FindElement(By.Id("divManualEntry")).FindElement(By.Id("btnManualEntry")).Click();
            //js.ExecuteScript("window.scrollTo(0, 300);"); 
            IWebElement manualEntry = driver.FindElement(By.Id("modalManualEntry"));

            if (manualEntry.GetAttribute("aria-hidden") == "true")
            {
                js.ExecuteScript("document.getElementById('btnManualEntry').click();");
                //js.ExecuteScript("var model = document.getElementById('modalManualEntry'); model.setAttribute('class','modal hide fade BiometricsModal in');model.setAttribute('aria-hidden','false');model.setAttribute('style','display: block;');");
            }
            //IWebElement tableContent = manualEntry.FindElement(By.XPath("//div[1]/div[2]"));
            Thread.Sleep(3000);
            IWebElement tableContent = manualEntry.FindElement(By.ClassName("modal-body")).FindElement(By.ClassName("tabbable")).FindElement(By.ClassName("tab-content"));
            js.ExecuteScript("window.scrollTo(0, 200);"); 
            tableContent.FindElement(By.Id("m_8")).Clear();
            Thread.Sleep(1000);
            tableContent.FindElement(By.Id("m_8")).SendKeys(weight);
            Thread.Sleep(1000);
            tableContent.FindElement(By.Id("m_13")).Clear();
            Thread.Sleep(1000);
            tableContent.FindElement(By.Id("m_13")).SendKeys(bfat);
            Thread.Sleep(1000);
            dr = driver;
            js = dr as IJavaScriptExecutor;
            js.ExecuteScript("return  VHM.BioM.btnStMeasurementsSave_onclick()");
            Thread.Sleep(4000);
            driver.FindElement(By.Id("btnSumClose")).Click();
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            wait.Until(d => d.FindElement(By.Id("btnManualEntry")));
            Thread.Sleep(3000);
            return driver.Title;
        }

        public static string getSupport(IWebDriver driver)
        {
            driver.Navigate().GoToUrl("https://mash.virginhealthmiles.com/secure/Help/ContactUsForm.aspx");
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            wait.Until(d => d.FindElement(By.Id("btnSbtQuestion")));
            Thread.Sleep(3000);
            string currentWindowHandle = driver.CurrentWindowHandle;
            driver.FindElement(By.Id("question")).SendKeys("how do I earn Healthmiles");
            IWebDriver dr = driver;
            IJavaScriptExecutor js = dr as IJavaScriptExecutor;
            js.ExecuteScript("javascript: submitQuestion();");
            Thread.Sleep(3000);
            var windowIterator = driver.WindowHandles;
            IWebDriver pop = null;
            foreach (var windowHandle in windowIterator)
            {
                pop = driver.SwitchTo().Window(windowHandle);

                if (pop.Title == "Virgin Healthmiles")
                    break;
            }
            pop.Close();
            Thread.Sleep(3000);
            driver.SwitchTo().Window(currentWindowHandle);
            Thread.Sleep(3000);
            return driver.Title;
        }

        public static string getNewsFeed(IWebDriver driver)
        {
            IWebElement header = driver.FindElement(By.ClassName("VHMheadermenuc"));
            //header.FindElement(By.XPath("//div[1]/ul/li[1]/ul/li[0]")).Click();
            IList<IWebElement> headermenu = header.FindElements(By.ClassName("menu-item-li"));
            headermenu[4].Click();
            IList<IWebElement> tabs = headermenu[4].FindElement(By.ClassName("dropdown-menu")).FindElements(By.TagName("li"));
            tabs[3].Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            wait.Until(d => d.FindElement(By.Id("msgInputBox")));
            return driver.Title;
        }

        public static string enterNewsFeed(IWebDriver driver, string date)
        {
            IWebDriver dr = driver;
            IJavaScriptExecutor js = dr as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollTo(0, 0);"); 
            IWebElement header = driver.FindElement(By.ClassName("VHMheadermenuc"));
            //header.FindElement(By.XPath("//div[1]/ul/li[1]/ul/li[0]")).Click();
            IList<IWebElement> headermenu = header.FindElements(By.ClassName("menu-item-li"));
            headermenu[4].Click();
            IList<IWebElement> tabs = headermenu[4].FindElement(By.ClassName("dropdown-menu")).FindElements(By.TagName("li"));
            tabs[3].Click();
            //WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            //wait.Until(d => d.FindElement(By.Id("msgInputBox")));
            Thread.Sleep(6000);
            driver.FindElement(By.Id("msgInputBox")).SendKeys(date);
            Thread.Sleep(1000);
            driver.FindElement(By.Id("msgPostButton")).Click();
            Thread.Sleep(1000);
            WebDriverWait enterwait = new WebDriverWait(driver, TimeSpan.FromSeconds(3));
            enterwait.Until(d => d.FindElement(By.Id("newsFeedContainer")));
            Thread.Sleep(1000);
            IList<IWebElement> newsfeeds = driver.FindElement(By.Id("newsFeedContainer")).FindElement(By.Id("newsFeedBody")).FindElement(By.Id("newsContainer")).FindElement(By.TagName("ul")).FindElements(By.TagName("li"));
            return newsfeeds[0].FindElement(By.ClassName("axero-section-list-content")).FindElement(By.ClassName("axero-section-list-content-title")).FindElement(By.ClassName("spanActionText")).Text;
        }

        public static void enterActivityJournal(IWebDriver driver)
        {
            IWebDriver dr = driver;
            IJavaScriptExecutor js = dr as IJavaScriptExecutor;
            Thread.Sleep(3000);
            js.ExecuteScript("location.reload();");
            Thread.Sleep(8000);
            IWebElement header = driver.FindElement(By.ClassName("VHMheadermenuc"));
            //header.FindElement(By.XPath("//div[1]/ul/li[1]/ul/li[0]")).Click();
            IList<IWebElement> headermenu = header.FindElements(By.ClassName("menu-item-li"));
            headermenu[1].Click();
            IList<IWebElement> tabs = headermenu[1].FindElement(By.ClassName("dropdown-menu")).FindElements(By.TagName("li"));
            tabs[0].Click();
            WebDriverWait activitywait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            activitywait.Until(d => d.FindElement(By.Id("tabs")));
            Thread.Sleep(3000);
            Random rn = new Random();
            //create activity entry
            int ractivity = rn.Next(2, 44);
            SelectElement acselect = new SelectElement(driver.FindElement(By.Id("AL-ActivityList")));
            acselect.SelectByIndex(ractivity);
            int rhour = rn.Next(0, 3);
            driver.FindElement(By.Id("txtALActivityHours")).SendKeys(rhour.ToString());
            Thread.Sleep(1000);
            int rmin = rn.Next(0, 60);
            driver.FindElement(By.Id("txtALActivityMinutes")).SendKeys(rmin.ToString());
            Thread.Sleep(1000);
            int rintensity = rn.Next(0, 3);
            SelectElement intensity = new SelectElement(driver.FindElement(By.Id("ddAlIntensity")));
            intensity.SelectByIndex(rintensity);
            if (driver.FindElement(By.Id("txtALSteps")).Displayed)
            {
                driver.FindElement(By.Id("txtALSteps")).SendKeys(rn.Next(3000, 6000).ToString());
                Thread.Sleep(1000);
            }
            driver.FindElement(By.Id("AL-btnSave")).Click();
            Thread.Sleep(3000);
        }

        public static string updateActivityJournal(IWebDriver driver, string randomActivity)
        {
            IWebElement header = driver.FindElement(By.ClassName("VHMheadermenuc"));
            //header.FindElement(By.XPath("//div[1]/ul/li[1]/ul/li[0]")).Click();
            //IList<IWebElement> headermenu = header.FindElements(By.ClassName("menu-item-li"));
            //headermenu[1].Click();
            //IList<IWebElement> tabs = headermenu[1].FindElement(By.ClassName("dropdown-menu")).FindElements(By.TagName("li"));
            //tabs[0].Click();
            //WebDriverWait activitywait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            //activitywait.Until(d => d.FindElement(By.Id("tabs")));
            IWebDriver dr = driver;
            IJavaScriptExecutor js = dr as IJavaScriptExecutor;
            js.ExecuteScript("VHM.AL.EditEntry(0,false);return false;");
            Thread.Sleep(3000);
            //driver.FindElement(By.Id("input-calories")).Clear();
            //Thread.Sleep(1000);
            driver.FindElement(By.Id("input-calories")).Clear();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("input-calories")).SendKeys(randomActivity);
            Thread.Sleep(1000);
            driver.FindElement(By.Id("AL-Save-Edit")).Click();
            Thread.Sleep(3000);
            js.ExecuteScript("window.scrollTo(0, 200);"); 
            //WebDriverWait enterwait = new WebDriverWait(driver, TimeSpan.FromSeconds(4));
            //enterwait.Until(d => d.FindElement(By.Id("view-row_0")));
            //compare the value
            IList<IWebElement> tds = driver.FindElement(By.Id("view-row_0")).FindElements(By.TagName("td"));
            string upCalAfter = tds[2].Text;
            return upCalAfter;
        }

    }
}
