﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using seleniumProject;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace test
{
    [TestFixture]
    public class SmokeTest
    {
        private IWebDriver driver;

        public SmokeTest()
        {
            driver = new FirefoxDriver();
            Thread.Sleep(5000);
            driver.Navigate().GoToUrl("https://mash.virginhealthmiles.com/login.aspx");
            WebDriverWait loginwait = new WebDriverWait(driver, TimeSpan.FromSeconds(3));
            //loginwait.Until(d => d.FindElement(By.Id("txtPlainPassword")));
            Thread.Sleep(8000);
            IWebElement login = driver.FindElement(By.Id("oUserID"));
            login.Clear();
            login.SendKeys("testsm0001");
            driver.FindElement(By.Id("txtPlainPassword")).SendKeys("a1111111");
            driver.FindElement(By.Id("oLogon")).Click();
           
            //WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
            //wait.Until(d => d.FindElement(By.Id("measurementSideNav")));
            Thread.Sleep(8000);
        }
        ~SmokeTest()
        {
            driver.Close();
        }
        //public void Dispose
        //{
        //    driver.Close();
        //}
        //[Test]
        //public void wholeTest()
        //{
        //    getMonthlyStatement_test();
        //    getShopForGoodPage_test();
        //    ActivityJournay_test();
        //    Thread.Sleep(5000);
        //    NewsFeed_test();
        //    //Thread.Sleep(5000);
        //    BioMeasurement_test();
        //    //Thread.Sleep(8000);
        //    Challenge_test();
        //    //Thread.Sleep(5000);
        //    Support_test();
        //}

        [Test]
        public void getShopForGoodPage_test()
        {
            string shopTitle = seleniumProject.Program.getShopForGoodPage(driver);
            StringAssert.AreEqualIgnoringCase("Virgin Pulse - Shop For Products", shopTitle);
        }

        [Test]
        public void getMonthlyStatement_test()
        {
            string monthTitle = seleniumProject.Program.getMonthStatementPage(driver);
            StringAssert.AreEqualIgnoringCase("Virgin Pulse -", monthTitle);
        }

        [Test]
        public void ActivityJournal_test()
        {
            seleniumProject.Program.enterActivityJournal(driver);
            Random rn = new Random();
            int rcalorie = rn.Next(50, 1000);
            string upcal = seleniumProject.Program.updateActivityJournal(driver, rcalorie.ToString());
            StringAssert.AreEqualIgnoringCase(rcalorie.ToString(), upcal);
        }

        [Test]
        public void NewsFeed_test()
        {
            string date = System.DateTime.Now.ToString();
            //seleniumProject.Program.getNewsFeed(driver);
            string post = seleniumProject.Program.enterNewsFeed(driver, date);
            StringAssert.AreEqualIgnoringCase(date, post);
        }

        [Test]
        public void BioMeasurement_test()
        {
            //Thread.Sleep(5000); 
            //seleniumProject.Program.getBioMeasurement(driver);
            Random rn = new Random();
            int rweight = rn.Next(120, 300);
            int rbfat = rn.Next(7, 30);
            string title = seleniumProject.Program.enterBioManualEntry(driver, rweight.ToString(), rbfat.ToString());
            //Thread.Sleep(8000);
            StringAssert.AreEqualIgnoringCase("Virgin Pulse - Bio Measurements", title);
        }

        [Test]
        public void Support_test()
        {
            string title = seleniumProject.Program.getSupport(driver);
            StringAssert.AreEqualIgnoringCase("Virgin Pulse - Contact Us", title);

        }
        //[Test]
        //public void Challenge_test1()
        //{
        //    string step1 = seleniumProject.Program.personalChallengeSteps1(driver);
        //    StringAssert.AreEqualIgnoringCase("Be the first to reach 100,000 steps", step1);
        //}
        //[Test]
        //public void Challenge_test2()
        //{
        //    string step2 = seleniumProject.Program.personalChallengeSteps2(driver, "smoke");
        //    StringAssert.AreEqualIgnoringCase("Name and Details", step2);
        //}
        //[Test]
        //public void Challenge_test3()
        //{
        //    string step3 = seleniumProject.Program.personalChallengeSteps3(driver);
        //    StringAssert.AreEqualIgnoringCase("Who would you like to invite?", step3);
        //}
        //[Test]
        //public void Challenge_test4()
        //{
        //    string step4 = seleniumProject.Program.personalChallengeSteps4(driver);
        //    StringAssert.AreEqualIgnoringCase("Ready to send?", step4);
        //}
        //[Test]
        //public void Challenge_test5()
        //{
        //    string header = seleniumProject.Program.personalChallengeSteps5(driver);
        //    System.Console.Write(header);
        //    StringAssert.AreEqualIgnoringCase("Virgin Pulse - Manage Your Challenge: smoke test", header);
        //}
        //[Test]
        //public void Challenge_test()
        //{
        //    string date = System.DateTime.Now.ToString();            
        //    string title = seleniumProject.Program.enterPersonalChallenge(driver, date);
        //    StringAssert.AreEqualIgnoringCase("Virgin Pulse - Manage Your Challenge: " + date + " test", title);
        //}
    }
}
